package hasher;

import hasher.HashOutput;
import java.util.Arrays;
import java.util.Iterator;

/**
 *
 * @author Elsk
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        MainPanel.main(args);
       new Test().main();
       
    }

    // TEST
    void main() {
        System.out.println();
//        try {
//            String a = "MD5";
//            Hasher h = Hasher.getHasher(new File("F:\\Downloads\\Matlab\\[矩阵实验室].Matlab_R2013b_Crack.rar"), a);       
//            Thread t = new HashThread(h, new HasherTextOutput(h.getFileName()));
//            t.start();
//        } catch (Exception ex) {
//            MainPanel.reportError(ex);
//        }
    }

    static class HasherTextOutput extends HashOutput {

        private final String fileName;

        public HasherTextOutput(String fileName) {
            this.fileName = fileName;
        }

        @Override
        public void updateResult(Object result) {
            System.out.printf("%s: %s%n", fileName, result);
        }
    }
}
