package hasher;

import hasher.Cell.FileCell;
import hasher.Cell.HashResultCell;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import static hasher.Cell.getCell;
import hasher.threads.ThreadScheduler;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

/**
 * The main GUI JPanel of the program. The {@link #main(java.lang.String[]) }
 * method displays the panel in a JFrame and display the JFrame on a
 * window-based operation system.
 *
 * @author Elsk
 */
public class MainPanel extends JPanel {

    //TODO clear/remove // Note the row number will change, same for names
    private static final String TITLE = "Elsk's File Hasher",
            ABOUT = "Elsk's File Hasher v1.0\nBuild Dec 15, 2013",
            HELP = "Everything is simple:\n"
            + "Just DRAG AND DROP some files!\n"
            + "The default algorithm is MD5.\n"
            + "Right click the records for more operations (rehash, copy, etc.)\n"
            + "You can also select multiple records and right click!";
    private static final String INIT_STRING = "Waiting...";
    private static final String[] COLUMN_NAMES = new String[]{"File", "Result"};
    private static final int FILE_COLUMN = 0, RESULT_COLUMN = 1; // Remember to change copy() when change columns
    /**
     * Visual Gap.
     */
    private static final int GAP = 20;
    /**
     * The max number of {@link HashThread} allowed.
     */
    private static final int MAX_THREADS = 15;
    /**
     * A mapping from row number to hash threads. Each HashThread will output
     * its resultCell to one row in the this.table. The number of HashThread in
     * this list should equal the number of rows in this.table.
     *
     * @see #table
     */
    private final Map<Integer, HashThread> hashThreads;
    private final ThreadScheduler scheduler;
    /**
     * The JTable displayed.
     */
    private final JTable table;
    /**
     * The {@link javax.swing.table.TableModel} used by {@link #table}.
     */
    private final DefaultTableModel model;

    public MainPanel() {
        model = new DefaultTableModel(new Object[0][COLUMN_NAMES.length], COLUMN_NAMES) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        hashThreads = new HashMap<>();
        scheduler = new ThreadScheduler(MAX_THREADS);
        table = new JTable(model);
        initGraphics();
        startScheduler();
    }

    /**
     * Initialize the GUI.
     */
    private void initGraphics() {
        /**
         * To add a new menu item. <br />
         * 1. Create a class that extends TableCellMenuItem. <br />
         * 2. In its constructor, write: <br />
         * super("text"); // text on the menu <br />
         * 3. Override the action() method to specify what to do.<br />
         * 4. Add a new instance in the following menuItems array.
         */
        new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, new DropTargetHandler());
        final JPopupMenu menu = new JPopupMenu();
        final TableCellMenuItem[] menuItems = {
            new ShowDetailsItem(),
            new CopyResultItem(),
            new CopyDetailsItem(),
            new CopyMD5SUMItem(),
            new RehashItem(),
            new CancelItem()
        };
//        final TableCellMenuItem showItem = menuItems[0];
        for (TableCellMenuItem item : menuItems) {
            menu.add(item);
        }
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int r = table.rowAtPoint(e.getPoint());
                int c = table.columnAtPoint(e.getPoint());
                if (r != -1 && c != -1) {
                    if (!table.getSelectionModel().isSelectedIndex(r)) {
                        table.setRowSelectionInterval(r, r);
                        table.setColumnSelectionInterval(c, c);
                    }
                    if (e.isPopupTrigger()) {
                        if (table.getSelectedRowCount() > 1) {
                            for (TableCellMenuItem item : menuItems) {
                                item.setEnabled(item.allowForMultipleRows());
                            }
                        } else if (table.getSelectedRowCount() == 1) {
                            for (TableCellMenuItem item : menuItems) {
                                item.setEnabled(item.allowForOneRow());
                            }
                        }
                        menu.show(e.getComponent(), e.getX(), e.getY());
                    }
                } else {
                    menu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        table.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                int r = table.getSelectedRow();
                if (r != -1) {
                    copy((List) model.getDataVector().get(r));
                }
            }//end actionPerformed(ActionEvent)
        }, "Copy", KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false), JComponent.WHEN_FOCUSED);
        table.setRowSelectionAllowed(true);
        table.setColumnSelectionAllowed(false);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
//        Font f = table.getFont();
//        table.setFont(new Font("Microsoft Jhenghei", f.getStyle(), f.getSize()));
        JScrollPane sp = new JScrollPane(table);
        final JPopupMenu menu2 = new JPopupMenu();
        JMenuItem aboutItem = new JMenuItem("About...");
        JMenuItem helpItem = new JMenuItem("Help...");
        aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                msgbox(ABOUT, "About");
            }
        });
        helpItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                msgbox(HELP, "Help");
            }
        });
        menu2.add(aboutItem);
        menu2.add(helpItem);
        sp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    menu2.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        SpringLayout l = new SpringLayout();
        l.putConstraint(SpringLayout.WEST, sp, GAP, SpringLayout.WEST, this);
        l.putConstraint(SpringLayout.EAST, this, GAP, SpringLayout.EAST, sp);
        l.putConstraint(SpringLayout.NORTH, sp, GAP, SpringLayout.NORTH, this);
        l.putConstraint(SpringLayout.SOUTH, this, GAP, SpringLayout.SOUTH, sp);
        this.setLayout(l);
        this.add(sp);
    }

    /**
     * Starts the {@link #scheduler}.
     */
    private void startScheduler() {
        scheduler.start();
    }

    // how to copy
    /**
     * Copy the string defined by
     * <code>o.toString()</code> to the system clipboard.
     *
     * @param o the object copied
     */
    private void copy(Object o) {
        String str = o.toString();
        if (!str.isEmpty()) {
            Toolkit.getDefaultToolkit().getSystemClipboard()
                    .setContents(new StringSelection(str), null);
        } else {
            this.msgbox("Nothing is copied. You've probably chosen uncompleted task(s).", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }

    // create new hash threads
    /**
     * Add a new row and start a new HashThread.
     */
    private void newHashThread(File f) {
        if (f.isFile()) {
            scheduler.schedule(newHashThread0(f));
        }
    }

    /**
     * Add several new rows and start new HashThreads.
     */
    private void newHashThread(Collection<? extends File> files) {
        List<Thread> toSchedule = new LinkedList<>();
        for (File f : files) {
            if (f.isFile()) { // TODO support folders
                toSchedule.add(newHashThread0(f));
            }
        }
        scheduler.schedule(toSchedule);
    }

    /**
     * Add a new row and start a new HashThread.
     */
    private void newHashThread(File f, String algorithm) {
        if (f.isFile()) {
            HashThread t = newHashThread0(f, algorithm);
            if (t != null) {
                scheduler.schedule(t);
            }
        }
    }

    /**
     * Add several new rows and start new HashThreads.
     */
    private void newHashThread(Collection<? extends File> files, String algorithm) {
        List<Thread> toSchedule = new LinkedList<>();
        for (File f : files) {
            if (f.isFile()) {
                HashThread t = newHashThread0(f, algorithm);
                if (t != null) {
                    toSchedule.add(t);
                }
            }
        }
        scheduler.schedule(toSchedule);
    }

    /**
     * Helper for newHashThread.
     */
    private HashThread newHashThread0(File f) {
        synchronized (model) {
            int rowNum = model.getRowCount();
            model.addRow(new Cell[]{getCell(f), getCell(INIT_STRING)});
            HashThread t = new GUIHashThread(rowNum, f, new GUIHashOutput(rowNum));
            hashThreads.put(rowNum, t);
            return t;
        }
    }

    /**
     * Helper for newHashThread.
     */
    private HashThread newHashThread0(File f, String algorithm) {
        synchronized (model) {
            int rowNum = model.getRowCount();
            model.addRow(new Cell[]{getCell(f), getCell(INIT_STRING)});
            GUIHashOutput output = new GUIHashOutput(rowNum);
            try {
                HashThread t = new GUIHashThread(rowNum, f, output, algorithm);
                hashThreads.put(rowNum, t);
                return t;
            } catch (NoSuchAlgorithmException ex) {
                output.updateResult(ex);
                return null;
            }
        }
    }

    // show messages
    /**
     * Display an exception in a {@link JOptionPane}, and print its stack trace
     * to standard error.
     *
     * @param ex the exception
     */
    public static void reportError(Exception ex) {
        ex.printStackTrace();
        staticMsgbox(ex.getMessage() + "\nSee Terminal for more information.", "Exception Occur!", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Brings up a dialog (in the middle of the screen) that displays a message
     * using a default icon determined by the
     * <code>messageType</code> parameter.
     *
     * @param message the <code>Object</code> to display
     * @param title the title string for the dialog
     * @param messageType the type of message to be displayed:
     * <code>ERROR_MESSAGE</code>, <code>INFORMATION_MESSAGE</code>,
     * <code>WARNING_MESSAGE</code>, <code>QUESTION_MESSAGE</code>, or
     * <code>PLAIN_MESSAGE</code>
     * @exception HeadlessException if
     * <code>GraphicsEnvironment.isHeadless</code> returns <code>true</code>
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JOptionPane#showMessageDialog(java.awt.Component, java.lang.Object,
     * java.lang.String, int)
     */
    private static void staticMsgbox(Object message, String title, int messageType) {
        JOptionPane.showMessageDialog(null, message, title, messageType);
    }

    /**
     * Brings up a dialog (in the middle of the panel) that displays a message
     * using a default icon determined by the
     * <code>messageType</code> parameter.
     *
     * @param message the <code>Object</code> to display
     * @param title the title string for the dialog
     * @param messageType the type of message to be displayed:
     * <code>ERROR_MESSAGE</code>, <code>INFORMATION_MESSAGE</code>,
     * <code>WARNING_MESSAGE</code>, <code>QUESTION_MESSAGE</code>, or
     * <code>PLAIN_MESSAGE</code>
     * @exception HeadlessException if
     * <code>GraphicsEnvironment.isHeadless</code> returns <code>true</code>
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JOptionPane#showMessageDialog(java.awt.Component, java.lang.Object,
     * java.lang.String, int)
     */
    private void msgbox(Object message, String title, int messageType) {
        JOptionPane.showMessageDialog(this, message, title, messageType);
    }

    /**
     * Brings up a plain message dialog (in the middle of the panel) that
     * displays a message using a default icon determined by the
     * <code>messageType</code> parameter.
     *
     * @param message the <code>Object</code> to display
     * @param title the title string for the dialog
     * @exception HeadlessException if
     * <code>GraphicsEnvironment.isHeadless</code> returns <code>true</code>
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JOptionPane#showMessageDialog(java.awt.Component, java.lang.Object,
     * java.lang.String, int)
     */
    private void msgbox(Object message, String title) {
        msgbox(message, title, JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Prompts the user for input in a blocking dialog where the
     * initial selection, possible selections, and all other options can
     * be specified. The user will able to choose from
     * <code>selectionValues</code>, where <code>null</code> implies the
     * user can input
     * whatever they wish, usually by means of a <code>JTextField</code>.
     * <code>initialSelectionValue</code> is the initial value to prompt
     * the user with. It is up to the UI to decide how best to represent
     * the <code>selectionValues</code>, but usually a
     * <code>JComboBox</code>, <code>JList</code>, or
     * <code>JTextField</code> will be used.
     *
     * @param message  the <code>Object</code> to display
     * @param title    the <code>String</code> to display in the
     *                  dialog title bar
     * @param selectionValues an array of <code>Object</code>s that
     *                  gives the possible selections
     * @param initialSelectionValue the value used to initialize the input
     *                 field
     * @return user's input, or <code>null</code> meaning the user
     *                  canceled the input
     * @exception HeadlessException if
     *   <code>GraphicsEnvironment.isHeadless</code> returns
     *   <code>true</code>
     * @see java.awt.GraphicsEnvironment#isHeadless
     */
    private String inputbox(Object message, String title, Object[] selectionValues,
            Object initialSelectionValue) {
        String s = (String) JOptionPane.showInputDialog(this,
                message, title, JOptionPane.PLAIN_MESSAGE, null, selectionValues,
                initialSelectionValue);
        return s;
    }

    /**
     * Get a row in the table.
     * @param r the row index
     * @return a {@link List} (hopefully a List of {@link Cell}s) that corresponds
     * to the content of each cell in the row
     */
    private List<Cell> row(int r) {
        return (List) model.getDataVector().get(r);
    }

    /**
     * Get the {@link FileCell} in the table.
     * @param r the row index
     * @return the {@link FileCell} specified.
     */
    private FileCell fileCell(int r) {
        return (FileCell) row(r).get(FILE_COLUMN);
    }
    /**
     * Get the {@link Cell} at the given row and the result column in the table.
     * @param r the row index
     * @return the {@link Cell} specified.
     */
    private Cell resultCell(int r) {
        return row(r).get(RESULT_COLUMN);
    }
    
    // menu items
    /**
     * A general menu item for the popup menu of {@link MainPanel#table}.
     */
    private abstract class TableCellMenuItem extends JMenuItem {

        /**
         * Creates a TableCellMenuItem with the specified text.
         * @param text the text of the TableCellMenuItem
         */
        TableCellMenuItem(String text) {
            super(text);
            this.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Cell cell = (Cell) table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
                    if (cell == null || cell.isEmpty()) {
                        msgbox("You've chosen an empty cell.", "Error!", JOptionPane.ERROR_MESSAGE);
                    } else {
                        action();
                    }
                }
            });
        }

        /**
         * The action performed when the menu item is selected and the selected
         * cell is not empty.
         */
        abstract void action();

        /**
         * Return whether this menu item should be enabled when only one row is
         * selected.
         */
        boolean allowForOneRow() {
            return true;
        }

        /**
         * Return whether this menu item should be enabled when multiple rows
         * are selected.
         */
        boolean allowForMultipleRows() {
            return true;
        }

        /**
         * Returns the indices of all selected rows.
         *
         * @return an array of integers containing the indices of all selected
         * rows, or an empty array if no row is selected
         */
        protected int[] rs() {
            return table.getSelectedRows();
        }

        /**
         * Return the {@link FileCell} at the selected row.
         * @see MainPanel#fileCell(int) 
         */
        protected FileCell fileCell() {
            return MainPanel.this.fileCell(table.getSelectedRow());
        }

        /**
         * Get the {@link Cell} at the selected row and the result column in the
         * table.
         * @see MainPanel#resultCell(int) 
         */
        protected Cell resultCell() {
            return MainPanel.this.resultCell(table.getSelectedRow());
        }
    }

    /**
     * A <code>TableCellMenuItem</code> for showing details of a record. It
     * is disabled when multiple rows are selected.
     */
    private class ShowDetailsItem extends TableCellMenuItem {

        ShowDetailsItem() {
            super("Show details...");
        }

        /**
         * Display a message box with the details in the row.
         */
        @Override
        void action() {
            Cell res = resultCell();
            msgbox(fileCell().toDetailedString() + "\n"
                    + res.toDetailedString().replace("\t", "        "),
                    "Details", res.msgType());
        }

        @Override
        boolean allowForMultipleRows() {
            return false;
        }
    }

    /**
     * A <code>TableCellMenuItem</code> for copying the result. It is disabled when
     * multiple rows are selected or the selected task has not finished.
     */
    private class CopyResultItem extends TableCellMenuItem {

        CopyResultItem() {
            super("Copy result");
        }

        @Override
        void action() {
            copy(resultCell().toString());
        }

        @Override
        boolean allowForOneRow() {
            return resultCell() instanceof HashResultCell;
        }

        @Override
        boolean allowForMultipleRows() {
            return false;
        }
    }

    /**
     * A <code>TableCellMenuItem</code> for copying details. 
     */
    private class CopyDetailsItem extends TableCellMenuItem {

        CopyDetailsItem() {
            super("Copy details to clipboard");
        }

        @Override
        void action() {
            StringBuilder sb = new StringBuilder();
            for (int r : rs()) {
                sb.append(row(r).get(FILE_COLUMN).toDetailedString())
                        .append("\n")
                        .append(row(r).get(RESULT_COLUMN).toDetailedString())
                        .append("\n");
            }
            copy(sb.toString());
        }
    }

    /**
     * A <code>TableCellMenuItem</code> for copying in md5sum format. 
     */
    private class CopyMD5SUMItem extends TableCellMenuItem {

        CopyMD5SUMItem() {
            super("Copy in md5sum format");
        }

        @Override
        void action() {
            StringBuilder sb = new StringBuilder();
            for (int r : rs()) {
                if (MainPanel.this.resultCell(r) instanceof HashResultCell) {
                    sb.append(MainPanel.this.resultCell(r).toString())
                            .append(" ")
                            .append(MainPanel.this.fileCell(r).toString())
                            .append("\n");
                }
            }
            copy(sb.toString());
        }
    }
    /**
     * A <code>TableCellMenuItem</code> for rehashing. 
     */
    private class RehashItem extends TableCellMenuItem {

        RehashItem() {
            super("Rehash with another algorithm...");
        }

        @Override
        public void action() {
//            List row = row();
            String prevAlg;
            try {
                prevAlg = ((HashResultCell) resultCell()).value().getAlgorithm();
            } catch (ClassCastException ex) {
                prevAlg = HashThread.DEFAULT_HASH;
            }
            String alg = inputbox("Please input the algorithm name.", "Choose Algorithm",
                    new String[]{"MD5", "SHA-256", "SHA-1"}, prevAlg);
            if (alg != null) {
                alg = alg.trim();
                if (alg.length() != 0) {
                    List<File> l = new ArrayList<>();
                    for (int r : rs()) {
                        l.add(MainPanel.this.fileCell(r).value());
                    }
                    newHashThread(l, alg);
                }
            }
        }
    }
    /**
     * A <code>TableCellMenuItem</code> for canceling tasks. 
     */
    private class CancelItem extends TableCellMenuItem {

        CancelItem() {
            super("Cancel");
        }

        @Override
        void action() {
            for (int r : rs()) {
                try {
                    hashThreads.get(r).kill();
                } catch (NullPointerException ex) {
                }
            }
        }
    }

    // output for hash thread
    /**
     * A <code>HashOutput</code> that directs the output to {@link MainPanel#table}.
     */
    private class GUIHashOutput extends HashOutput {

        /**
         * To which row the result is written.
         */
        private int rowIndex;

        public GUIHashOutput(int rowIndex) {
            this.rowIndex = rowIndex;
        }

        @Override
        void updateProgress(double progress) {
            updateResult(progress);
        }

        /**
         * Wrap the resultCell object as a cell and display it at
         * {@link #rowIndex}, {@link MainPanel#RESULT_COLUMN}.
         *
         * @param result
         */
        @Override
        public void updateResult(Object result) {
            model.setValueAt(getCell(result), rowIndex, RESULT_COLUMN);
        }
    }

    // dropping files
    /**
     * Support dropping files for the panel.
     */
    private class DropTargetHandler extends DropTargetAdapter {

        @Override
        public void drop(DropTargetDropEvent dtde) {

            Transferable transferable = dtde.getTransferable();
            if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                dtde.acceptDrop(dtde.getDropAction());
                try {
                    List<File> transferData = (List) transferable.getTransferData(DataFlavor.javaFileListFlavor);
                    if (transferData != null && transferData.size() > 0) {
                        droppedFiles(transferData);
                        dtde.dropComplete(true);
                    }
                } catch (UnsupportedFlavorException | IOException ex) {
                    reportError(ex);
                }
            } else {
                dtde.rejectDrop();
            }
        }

        /**
         * Invoked when files are dropped onto the panel.
         */
        protected void droppedFiles(final List<File> files) {
            new Thread(){
                @Override
                public void run() {
                    newHashThread(files);
                }
            }.start();
        }
    }

    /**
     * A hash thread that knows how to remove itself from
     * <code>MainPanel.this.hashThreads</code>.
     *
     * @see MainPanel#hashThreads
     */
    private class GUIHashThread extends HashThread {

        private int row;

        public GUIHashThread(int row, File f, HashOutput output) {
            super(f, output);
            this.row = row;
        }

        public GUIHashThread(int row, File f, HashOutput output, String algorithm)
                throws NoSuchAlgorithmException {
            super(f, output, algorithm);
            this.row = row;
        }

        @Override
        public void removeReference() {
            super.removeReference();
            hashThreads.remove(row);
        }
    }

    /**
     * Main entry of program. Uses a JFrame to display the panel, and uses
     * Nimbus Look and Feel.
     *
     * @see javax.swing.UIManager
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */

        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    reportError(ex);
                }
            }
        }
        JFrame frame;
        frame = new JFrame(TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new MainPanel();
        frame.setContentPane(panel);
        frame.setPreferredSize(new Dimension(800, 500));
        frame.pack();
        frame.setVisible(true);
        //        while (true) {
        //            try {
        //                Thread.sleep(1000);
        ////                Thread[] tarray = new Thread[Thread.activeCount()];Thread.enumerate(tarray);
        ////                System.out.printf("%d Active threads: %s%n",tarray.length, Arrays.toString(tarray));
        //                System.out.printf("%d Active threads%n", Thread.activeCount());
        //                System.out.flush();
        //            } catch (InterruptedException ex) {
        //                break;
        //        } // DEBUG
        //        } // DEBUG

    }
}