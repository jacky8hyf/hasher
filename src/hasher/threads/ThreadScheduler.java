package hasher.threads;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A ThreadScheduler helps schedule threads to run. <p />
 * At any given time, a ThreadScheduler would only allow a certain number (given
 * at initialization) of instances of threads to run. Whenever new threads are
 * scheduled (by invoking the
 * <code>schedule()</code> method) the ThreadScheduler will attempt to put it in
 * the thread pool. If the thread pool is already full, the ThreadScheduler will
 * check all threads (under its control), and remove any dead threads, yielding
 * space for the new thread. If no dead threads are found, it will wait for a
 * certain time and check again. A thread put into the thread pool will start
 * immediately.
 *
 * @see #schedule(java.lang.Thread) 
 * @see #schedule(java.util.Collection) 
 * @see #REFRESH
 * @author Elsk
 */
public class ThreadScheduler extends Thread {

    /**
     * The thread pool containing all running threads under control.
     */
    private final Set<Thread> running;
    /**
     * When attempting to clean up dead threads from the thread pool, if no threads
     * are removed (ie all threads are alive), <code>REFRESH</code> defines
     * the time(in milliseconds) for the next attempt.
     */
    private static final int REFRESH = 1000;
    /**
     * The queue of scheduled threads.
     */
    private final ThreadMailbox toRun;
    /**
     * Max number of instances allowed in the thread pool.
     */
    private final int maxNum;

    /**
     * A ThreadScheduler that allows a certain number of instances of threads
     * to run concurrently.
     * @param max max number of instances allowed in the thread pool.
     */
    public ThreadScheduler(int max) {
        super("ThreadScheduler");
        running = new HashSet<>(max, 1.2f);
        toRun = new ThreadMailbox();
        this.setPriority(MIN_PRIORITY);
        this.maxNum = max;
    }

    /**
     * Schedule a thread to run.
     * @param t the thread
     */
    public void schedule(Thread t) {
//        System.out.printf("Scheduling: %s%n", t);
        toRun.deposit(t);
    }

    /**
     * Schedule multiple threads to run
     * @param threads a collection of threads to run
     */
    public void schedule(Collection<? extends Thread> threads) {
//        System.out.printf("Scheduling: %d threads%n", toAdd.size()); // DEBUG
        toRun.deposit(threads);
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread t = toRun.receive();
//                System.out.printf("Received: %s%n", t);
                boolean removed = false;
                while (running.size() > maxNum) {
//                    System.out.println("Attmepting to remove from running");
                    for (Iterator<Thread> itr = running.iterator(); itr.hasNext();) {
                        Thread r = itr.next();
                        if (!r.isAlive()) {
                            itr.remove();
                            removed = true;
                        }
                    }
                    if(!removed) 
                        Thread.sleep(REFRESH);
                }
                Thread.sleep(0); // Allow for the garbage collector to clean up removed threads.
                running.add(t);
                t.start();
            } catch (InterruptedException ex) {
                break;
            }
        }
    }
}
