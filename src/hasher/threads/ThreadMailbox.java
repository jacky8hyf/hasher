package hasher.threads;

import java.util.Collection;
import java.util.LinkedList;

/**
 * A simple mailbox in which elements are threads. This mailbox maintains a FIFO
 * (first in, first out) queue; threads deposited earlier will always be
 * "received" before threads deposited later.
 *
 * @author Elsk
 */
class ThreadMailbox {

    private final LinkedList<Thread> threads;

    ThreadMailbox() {
        threads = new LinkedList<>();
    }

    /**
     * Put the thread in queue and notify receivers to receive the thread.
     *
     * @param t the thread deposited
     */
    synchronized void deposit(Thread t) {
        threads.add(t);
        notify();
    }

    /**
     * Put multiple threads in the queue (in the order defined by
     * {@link Collection#addAll(java.util.Collection)}) and notify receivers to
     * receive these threads.
     *
     * @param toAdd the threads deposited
     */
    synchronized void deposit(Collection<? extends Thread> toAdd) {
        if (toAdd.size() > 0) {
            threads.addAll(toAdd);
            notify();
        }
    }

    /**
     * A receiver will call this method (probably in an infinite loop) to
     * receive threads when they are deposited.
     *
     * @return the threads received by the receiver
     * @throws InterruptedException if the receiver's thread is interrupted
     */
    synchronized Thread receive() throws InterruptedException {
        while (threads.isEmpty()) {
            wait();
        }
        return threads.removeFirst();
    }
}