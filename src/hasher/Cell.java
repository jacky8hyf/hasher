package hasher;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.swing.JOptionPane;

/**
 * A Cell is a wrapper for any object that produces specific strings for
 * specific type of objects.
 *
 * @author Elsk
 */
public abstract class Cell<T> {

    /**
     * To create a new Cell type. <br />
     * 1. Create a new class that extend the Cell class.<br />
     * 2. Add instanceof statement at getCell(Object). (Mind the order)
     */
    /**
     * The wrapped object.
     */
    protected T o;

    private Cell(T o) {
        this.o = o;
    }

    /**
     * Return a new cell that is most suitable for the given object,
     * according to its type (Type dispatching).
     * @param o the wrapped object
     * @return the new cell
     */
    public static Cell getCell(Object o) {
        if (o instanceof Double) {
            return new ProgressCell((Double) o);
        }
        if (o instanceof File) {
            return new FileCell((File) o);
        }
        if (o instanceof InterruptedException) {
            return new InterruptedExceptionCell((InterruptedException) o);
        }
        if (o instanceof Exception) {
            return new ExceptionCell((Exception) o);
        }
        if (o instanceof HashResult) {
            return new HashResultCell((HashResult) o);
        }
        return new ObjectCell(o);
    }

    /**
     * A short display string in the table cell.
     */
    @Override
    public String toString() {
        return isEmpty() ? "" : o.toString();
    }

    /**
     * A long detailed string displayed in the message box.
     */
    public String toDetailedString() {
        return isEmpty() ? "" : o.toString();
    }

    /**
     * Message box type.
     */
    public int msgType() {
        return JOptionPane.PLAIN_MESSAGE;
    }

    /**
     * Return true if the wrapped object is null, false otherwise.
     */
    public boolean isEmpty() {
        return o == null;
    }

    /**
     * Return the wrapped object.
     */
    public T value() {
        return o;
    }

    /**
     * A cell that contains any object.
     */
    public static class ObjectCell extends Cell<Object> {

        private ObjectCell(Object o) {
            super(o);
        }
    }

    /**
     * A cell that contains a {@link File}.
     */
    public static class FileCell extends Cell<File> {

        private FileCell(File o) {
            super(o);
        }

        @Override
        public String toString() {
            return isEmpty() ? "" : o.getName();
        }
    }

    /**
     * A cell that contains an exception.
     */
    public static class ExceptionCell extends Cell<Exception> {

        private ExceptionCell(Exception o) {
            super(o);
        }

        @Override
        public String toString() {
            return isEmpty() ? "" : "Error occured. Right click for details..." //                    o.getClass().getSimpleName()
                    ;
        }

        @Override
        public String toDetailedString() {
            StringWriter sw = new StringWriter();
            try (PrintWriter pw = new PrintWriter(sw)) {
                o.printStackTrace(pw);
            }
            return sw.toString();
        }

//        @Override
//        public String title() {
//            return "Exception";
//        }
        @Override
        public int msgType() {
            return JOptionPane.ERROR_MESSAGE;

        }
    }

    /**
     * A cell that contains an {@link InterruptedException}.
     */
    public static class InterruptedExceptionCell extends ExceptionCell {

        private InterruptedExceptionCell(InterruptedException o) {
            super(o);
        }

        @Override
        public String toString() {
            return isEmpty() ? "" : "Stopped";
        }

        @Override
        public String toDetailedString() {
            return "Cancelled by user";
        }
    }

    /**
     * A cell that contains a {@link HashResult}.
     */
    public static class HashResultCell extends Cell<HashResult> {

        private HashResultCell(HashResult o) {
            super(o);
        }

        @Override
        public String toDetailedString() {
            return o.toDetailedString();
        }
    }

    /**
     * A cell that contains a floating point value representing the progress of
     * hashing.
     */
    public static class ProgressCell extends Cell<Double> {

        private ProgressCell(Double o) {
            super(o);
        }

        @Override
        public String toString() {
            int bars = (int) Math.round(o * 100);
            StringBuilder b = new StringBuilder(110);
            for (int _ = 0; _ < bars; _++) {
                b.append("|");
            }
            b.append(String.format("%.2f%%", o * 100));
            return b.toString();
        }

        @Override
        public String toDetailedString() {
            return String.format("%.2f%%", o * 100) + " finished";
        }
    }
}
