
package hasher;

/**
 * A two-string tuple containing the result and the algorithm,
 * @author Elsk
 */
public class HashResult {

    private final String result;
    private final String algorithm;

    public HashResult(String result, String algorithm) {
        this.result = result;
        this.algorithm = algorithm;
    }

    public String getResult() {
        return result;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    @Override
    public String toString() {
        return result;
    }
    public String toDetailedString() {
        return String.format("%s: %s", algorithm, result);
    }
    
}
