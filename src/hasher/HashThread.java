package hasher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * A HashThread uses a pair of threads to manage the process of hashing a file.
 * <p />
 * At any given time, there are in effect two threads controlled by a HashThread
 * object. <p />
 * 1. The <b>hashing thread</b> is responsible for computing the hash value of
 * the file, until interrupted.<br />
 * 2. The <b>display thread</b> is responsible for retrieving the progress of
 * hashing and displaying it to output, until interrupted. When interrupted, it
 * will display an InterruptedException in the output. (The output is a
 * {@link HashOutput} object.)<p />
 * In the implementation, the display thread is just
 * <code>this</code>. However, when
 * <code>this</code> is referred to as a THREAD,
 * <code>displayThread</code> is used; when
 * <code>this</code> is referred to as the pair of threads responsible for the
 * whole hashing process,
 * <code>this</code> is (implicitly) used. <p />
 * Calling
 * <code>start()</code> will cause both threads to start. Calling
 * <code>kill()</code> will cause both threads to be interrupted (hence stopped).
 *
 * @author Elsk
 */
public class HashThread extends Thread {

    public static final String DEFAULT_HASH = "MD5";
    private final Hasher hasher;
    protected final HashOutput output;
    /**
     * Thread that is responsible for hashing.
     */
    private final Thread hashingThread;
    /**
     * Thread that is responsible for displaying information. (A synonym to
     * <code>this</code>.)
     */
    private final Thread displayThread;

    public HashThread(File f, HashOutput output) {
        super(f.getName() + "_disp");
        this.output = output;
        this.hasher = getHasher(f);
        this.hashingThread = new Thread(this.hasher, f.getName() + "_hash");
        this.displayThread = this;
    }

    public HashThread(File f, HashOutput output, String algorithm) throws NoSuchAlgorithmException {
        super(f.getName() + "_disp");
        this.output = output;
        this.hasher = getHasher(f, algorithm);
        this.hashingThread = new Thread(this.hasher, f.getName() + "_hash");
        this.displayThread = this;
    } // TODO remove repeated constructor code

    /**
     * {@inheritDoc }
     * Defines what the display thread does.
     */
    @Override
    public void run() {
        hashingThread.start();
        Exception e = null;
        while (!hasher.done()) {
//            System.out.println(hasher.resultBytes); //DEBUG
            try {
                output.updateProgress(hasher.getProgress()); // will wait until new percentage update.
            } catch (InterruptedException ex) {
                e = this.hasher.exception; // interrupted from Hasher.run(). Remember this exception
                break;
            }
        }
        try {
            String hashStr;
            if (e == null) { // Former while loop not terminated by exception
                synchronized (hasher) {
                    hashStr = hasher.getHashString(); // first attempt
                    while (hashStr == null) { // attempt unsuccessful
//                        System.out.println("Locking..."); //DEBUG
                        hasher.wait(); //Notified by hasher.generateHashString, when hash value is ready
//                        System.out.println("unlocked.");//DEBUG
                        hashStr = hasher.getHashString();
                    }
                    output.updateResult(new HashResult(hashStr, this.hasher.md.getAlgorithm()));
                }
            } else {
                throw e;
            }
        } catch (Exception ex) {
//            MainPanel.reportError(ex);
            output.updateResult(ex);
        }
        this.removeReference();
    }

    /**
     * Kill both threads in effect.
     */
    public void kill() {
        /*
         * Interrupting the sole hashing thread is expected to cause
         * this.hasher.createChecksum() to throw an InterruptedException, which
         * is caught in this.hasher.run() and cause the display thread to be interrupted.
         * Interrupting the display thread will raise an InterruptedException in this.hasher.checker.receive(),
         * which is passed to this.hasher.getProgress and then to this.displayThread.run() and is displayed
         * in this.output.
         */
        hashingThread.interrupt();// interrupt the hashing thread.
    }
    /**
     * Overriden by subclasses to remove all reference to this so that 
     * the garbage collector will collect <code>this</code>.
     */
    public void removeReference() {        
    }

    private Hasher getHasher(File file, String algorithm) throws NoSuchAlgorithmException {
        return new Hasher(file, algorithm);
    }

    private Hasher getHasher(File fileName) {
        try {
            return new Hasher(fileName, DEFAULT_HASH);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * A Hasher takes a file and hash it with certain algorithm. The default is
     * MD5. It will LOCK the thread that called
     * <code>run()</code> after the whole file is processed and before the hash
     * value is actually calculated (by {@link MessageDigest#digest() }).
     *
     * @author Elsk
     */
    private class Hasher implements Runnable {

        private final File file;
        private final long size;
        /**
         * Mailbox.
         */
        private final ProgressChecker checker;
        private final MessageDigest md;
        /**
         * The raw bytes of the final hash string. It is null when processing
         * the whole file is not yet finished.
         */
        private byte[] resultBytes;
        private String resultHashString;
        /**
         * The exception that occurred in
         * <code>run()</code>, null if no exception occurred.
         */
        private Exception exception;
        private static final int BUFFERSIZE = 8 * 1024 * 1024;

        private Hasher(File file, String algorithm)
                throws NoSuchAlgorithmException {
            this.file = file;
            size = file.length();
            checker = new ProgressChecker();
            md = MessageDigest.getInstance(algorithm);
            resultBytes = null;
            resultHashString = null;
        }

//    private Hasher(String fileName, String algorithm)
//            throws NoSuchAlgorithmException {
//        this(new File(fileName), algorithm);
//    }
        /**
         * {@inheritDoc }
         * Defines what the hashing thread does.
         */
        @Override
        public void run() {
            try {
                createChecksum();
                generateHashString();
            } catch (InterruptedException | IOException ex) {
                exception = ex;
                displayThread.interrupt(); //Interrupt the display thread. Correspond to wait() in ProgressChecker.receive
            }
        }

        /**
         * Create the raw hash value and store it to resultBytes. This method
         * should be (indirectly) invoked by hashing thread.
         *
         * @throws InterruptedException when
         * <code>HashThread.this.hashingThread.isInterrupted()</code> (by
         * HashThread.this.kill())<br/>
         * @see #kill()
         * @see #hashingThread
         *
         */
        private void createChecksum()
                throws IOException, InterruptedException {
            try (InputStream fis = new FileInputStream(file)) {
                byte[] buffer = new byte[BUFFERSIZE];

                int numRead;

                do {
                    numRead = fis.read(buffer);
                    if (numRead > 0) {
                        md.update(buffer, 0, numRead);
                        this.checker.incProgress();
                    }
                    if (hashingThread.isInterrupted()) {
                        InterruptedException ex = new InterruptedException();
                        ex.setStackTrace(hashingThread.getStackTrace());
                        throw ex;
                    }
                } while (numRead != -1 && !hashingThread.isInterrupted());
            }
            resultBytes = md.digest();
        }

        /**
         * Return processed percentage. It is invoked by display thread.
         *
         * @throws InterruptedException
         */
        private double getProgress() throws InterruptedException {
            return this.checker.receive();
        }

        /**
         * Return true when the whole file are processed, even before the hash
         * string is produced.
         */
        private synchronized boolean done() {
            return this.checker.prev == this.size; //this.resultBytes != null;
        }

        /**
         * Locks the (hashing) thread (with synchronized keyword), attempts to
         * generate the the hash string calculated from the raw bytes and notify
         * all threads to come get the hash string, and releases the lock. This
         * is invoked by
         * <code>run()</code> that is invoked by the hashing thread.
         */
        private synchronized void generateHashString() {
            if (resultBytes == null) {
                return;
            }
            StringBuilder s = new StringBuilder();

            for (int i = 0; i < resultBytes.length; i++) {
                s.append(Integer.toString((resultBytes[i] & 0xff) + 0x100, 16).substring(1).toUpperCase());
            }
            resultHashString = s.toString();
//        System.out.println("notifying...");//DEBUG
            this.notifyAll(); // Each thread that is locked by me should wake up and grab
            // the hash string from getHashString()
        }

        /**
         * Returns the hash string calculated from the raw bytes, or null when
         * the hash string is not ready.
         */
        public synchronized String getHashString() {
//        if (resultHashString == null) {
//            throw new IllegalStateException("hash string not generated");
//        }
            return resultHashString;
        }

        /**
         * A one-element mailbox. It will lock the thread that calls receive()
         * when no value is available, and wake up the thread that call
         * receive() when another thread deposit a value.
         */
        private class ProgressChecker {

            private boolean hasValue;
            private long prev;
//        private final long diff = Hasher.this.size / 200;
            private final double dSize = Hasher.this.size;

            /**
             * Increase the value in the mailbox by {@link Hasher#BUFFERSIZE},
             * and notify all threads locked by
             * <code>receive()</code>. It is invoked by hashing thread.
             */
            private void incProgress() {
                this.deposit(Math.min(this.prev + Hasher.BUFFERSIZE, Hasher.this.size));
            }

            /**
             * Deposit D. It is invoked by hashing thread.
             */
            private synchronized void deposit(long d) {
//                System.out.printf("%s Deposit %s %d%n", Thread.currentThread(), file.getName(), d);//DEBUG
//            if (Math.abs(d - this.prev) < this.diff) return; // notify only on at least one percentage.
                this.prev = d;
                this.hasValue = true;
                this.notify(); // corresponding to the wait() in receive
            }

            /**
             * Return the percentage. It is invoked by display thread. Lock the
             * thread when no value is available.
             */
            private synchronized double receive() throws InterruptedException {
//                System.out.printf("%s Receiving %s...%n",Thread.currentThread(), file.getName());//DEBUG
                while (!hasValue) {
                    this.wait(); // corresponding to the notify() in deposit.
                    // Interruption denotes an exception in Hasher.run
                }
                hasValue = false;
//            System.out.printf("%s Received %f%n", file.getName(), this.prev / this.dSize);//DEBUG
                return this.prev / this.dSize;
            }
        }
    }
}
