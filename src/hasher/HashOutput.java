package hasher;

/**
 * Output for {@link HashThread}.
 *
 * @author Elsk
 */
public abstract class HashOutput {

    /**
     * Display a progress.
     *
     * @param progress a floating point number in the range [0.0, 1.0]
     */
    void updateProgress(double progress) {
        updateResult(String.format("%.2f%%", progress * 100));
    }

    /**
     * Display a result.
     */
    abstract void updateResult(Object result);
}
